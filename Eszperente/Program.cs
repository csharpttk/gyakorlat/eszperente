﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eszperente
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Kérek egy mondatot és megmondom eszperente-e...");
            string szöveg = Console.ReadLine();
            if (szöveg.eszperente_e())
                Console.WriteLine("Eszperente - csupa e");
            else
                Console.WriteLine("Nem eszperente...");
        }
    }
}
