﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eszperente
{
    static class Eszperente
    {
        public static bool eszperente_e(this string szöveg)
        {
            szöveg = szöveg.ToLower(); //ne kelljen külön nagy betűkre vizsgálni
            string mgh = "éiíuúöőüűaá"; //e kivételével a magánhangzók
            bool eszp = true;
            int i = 0;
            while (i<szöveg.Length && eszp)
            {
                eszp = eszp && !mgh.Contains(szöveg[i++]);
            }
            return eszp;
        }
    }
}
